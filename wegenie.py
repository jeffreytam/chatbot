#!/usr/bin/python3
# -*- coding: utf-8 -*-

import requests
import logging
import json
# import math
import time
import datetime
# import random
import pymysql.cursors
from config import *


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def popMessages(size):
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `receive_message` WHERE processed IS NULL ORDER BY `time` ASC LIMIT %s" % size
            cursor.execute(sql)
            resultset = cursor.fetchall()
            if (resultset):
                for res in resultset:
                    sid = res['sid']
                    sql = "UPDATE `receive_message` SET `processed` = CURRENT_TIMESTAMP WHERE `receive_message`.`sid` = %s" % sid
                    cursor.execute(sql)
                connection.commit()
    finally:
        connection.close()
    return resultset

def popEvents(size):
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `event` WHERE processed IS NULL ORDER BY `time` ASC LIMIT %s" % size
            cursor.execute(sql)
            resultset = cursor.fetchall()
            if (resultset):
                for res in resultset:
                    sid = res['sid']
                    sql = "UPDATE `event` SET `processed` = CURRENT_TIMESTAMP WHERE `event`.`sid` = %s" % sid
                    cursor.execute(sql)
                connection.commit()
    finally:
        connection.close()
    return resultset


def getRevokedMsg(revoke_id, client):
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `receive_message` WHERE `id` LIKE '{0}' AND `client` = '{1}' ORDER BY `sid` DESC LIMIT 1".format(revoke_id, client)
            cursor.execute(sql)
            result = cursor.fetchone()
    finally:
        connection.close()

    return result

def sendRevokedTextMsg(message):
    logger.info('sending revoked text message')
    pass

def sendRevokedMediaMsg(message):
    logger.info('sending revoked media message')
    pass

def sendRevokedAppMsg(message):
    logger.info('sending revoked app message')
    pass

def sendRevokedCardMsg(message):
    logger.info('sending revoked card message')
    pass

def sendFriendMessage(client, id, content=None, media_path=None):
    data={"id": id}
    if (content):
        data['content'] = content
    if (media_path):
        data['media_path'] = media_path
    try:
        response = requests.post(
            url="http://localhost:2000/openwx/send_friend_message",
            params={
                "client": client,
            },
            headers={
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
            },
            data = data,
        )
        logger.debug('Response HTTP Status Code: {status_code}'.format(
            status_code=response.status_code))
        logger.debug('Response HTTP Response Body: {content}'.format(
            content=response.content))
    except requests.exceptions.RequestException:
        logger.error('HTTP Request failed')



def main():
    connection = pymysql.connect(host=dbserver,
                                 user=dbuser,
                                 password=dbpass,
                                 db=dbname,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    try:
        while (True):
            messageset = popMessages(10)
            if (messageset):
                for message in messageset:
                    logger.info("Message {0} {1}".format(str(message['time']), message['format']))
                    if (message['format'] == 'revoke'):
                        logger.info('Revoked message')
                        msg = json.loads(message['raw'])
                        logger.info("revoke id: %s" % msg['revoke_id'])
                        client = message['client']
                        result = getRevokedMsg(msg['revoke_id'], client)

                        if (result == None):
                            logger.info("Cannot find revoked message")
                            continue

                        revoke_time = result['time']
                        raw = json.loads(result['raw'])

                        sender = raw['sender']
                        original_time = datetime.datetime.fromtimestamp(int(raw['time'])).strftime('%H:%M:%S')
                        format = raw['format']
                        type = raw['type']

                        if (type == 'group_message'):
                            groupname = raw['group_name']
                            notice = "以上是一条撤回消息\n来自「{0}」群的「{1}」\n发送于 {2}".format(groupname, sender, original_time)
                        elif (type == 'friend_message'):
                            notice = "以上是一条撤回消息\n来自好友「{0}」\n发送于 {1}".format(sender, original_time)

                        if (format == 'text'):
                            content = raw['content']
                            logger.info('content %s' % content)
                            sendFriendMessage(client, 'filehelper', content=content)
                        elif (format == 'media'):
                            media_path = raw['media_path']
                            logger.info('media_path %s' % media_path)
                            if (media_path != "non-exist-path"):
                                sendFriendMessage(client, 'filehelper', media_path=media_path)
                            else:
                                sendFriendMessage(client, 'filehelper', content="[该视频无法显示]")

                        sendFriendMessage(client, 'filehelper', content = notice)
                    elif (message['format'] == 'text'):
                        pass
                        # if (message['type'] == 'group_notice'):
                        #     logger.info('Group notice')
                        #     msg = json.loads(message['raw'])
                        #     client = message['client']
                        #     raw = json.loads(result['raw'])
                        #     original_time = datetime.datetime.fromtimestamp(int(raw['time'])).strftime('%H:%M:%S')
                        #     groupname = raw['group_name']
                        #     content = raw['content']
                        #     notice = "【群精灵提醒】\n「{0}」群中发生事件\n{1}".format(groupname, content)
                        #     sendFriendMessage(client, 'filehelper', content = notice)

            else:
                # logger.info("no new message")
                pass

            eventset = popEvents(5)
            if (eventset):
                for event in eventset:
                    logger.info("Event {0} {1}".format(str(event['time']), event['event']))
                    client = event['client']
                    data = json.loads(event['params'])
                    
                    if (event['event'] == 'new_group_member'):
                        member = data[0]
                        group = data[1]
                        notice = "「{0}」加入了「{1}」群".format(member['displayname'], group['displayname'])
                        sendFriendMessage(client, 'filehelper', content = notice)
                    elif (event['event'] == 'lose_group_member'):
                        member = data[0]
                        group = data[1]
                        notice = "「{0}」离开了「{1}」群".format(member['displayname'], group['displayname'])
                        sendFriendMessage(client, 'filehelper', content = notice)



            else:
                # logger.info("no new event")
                pass

            time.sleep(10)

    except Exception:
        pass

if __name__ == '__main__':
    main()

