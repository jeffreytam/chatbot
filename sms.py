# -*- coding: utf-8 -*-
import requests
import datetime
import hashlib


def sign(secret, parameters):
    if hasattr(parameters, "items"):
        keys = sorted(parameters)
        parameters = "%s%s%s" % (secret, str().join('%s%s' % (key, parameters[key]) for key in keys), secret)
    sign = hashlib.md5(parameters.encode('utf-8')).hexdigest().upper()
    return sign

def sendSMS(user_params, secret):
    signed_params = user_params.copy()
    signed_params["sign"] = sign(secret, signed_params)

    try:
        response = requests.post(
            url="https://eco.taobao.com/router/rest",
            params = signed_params,
        )
        # print('Response HTTP Status Code: {status_code}'.format(status_code=response.status_code))
        # print('Response HTTP Response Body: {content}'.format(content=response.content))
    except requests.exceptions.RequestException:
        print('HTTP Request failed')
        return False

    return response.content.decode("utf-8")

def main():
    code = "654321"
    app_key = "23616814"
    secret = "af2200eac4b7f76745a3e1e1347dab02"
    method = "alibaba.aliqin.fc.sms.num.send"
    sign_method = "md5"
    version = "2.0"
    sms_param = "{code:'%s'}" % code

    user_params = {
        "extend": "",
        "sms_type": "normal",
        "sms_free_sign_name": "群精灵",
        "rec_num": "15802199990",
        "sms_template_code": "SMS_44440005",
        "sms_param": sms_param,
        "app_key": app_key,
        "method": method,
        "sign_method": sign_method,
        "v": version,
        "format": "json",
        "timestamp": datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
    }

    response = sendSMS(user_params,secret)
    print(response)


if __name__ == '__main__':
    main()

