<?php
include __DIR__ . '/config.php';
include __DIR__ . '/functions.php';

$conn = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_GET['uid']) && isset($_GET['timestamp'])) {
    $uid = $_GET['uid'];
    $timestamp = $_GET['timestamp'];

    $codesent = false;
    $scanned = false;
    $confirmed = false;
    $successful = false;
    $failed = false;

    $sql = "SELECT * FROM `event` WHERE client = '{$uid}' AND time >= FROM_UNIXTIME('{$timestamp}') ORDER BY sid DESC";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            if ($row['params'] == "[\"loading\",\"scaning\"]") {
                $codesent = true;
            }
            if ($row['params'] == "[\"scaning\",\"confirming\"]") {
                $scanned = true;
            }
            if ($row['params'] == "[\"confirming\",\"updating\"]") {
                $confirmed = true;
            }
            if ($row['params'] == "[\"updating\",\"running\"]") {
                $successful = true;
            }
            if ($row['params'] == "[\"updating\",\"scaning\"]") {
                $failed = true;
            }
        }
        
    }
    $result = $codesent * 10000 + $scanned * 1000 + $confirmed * 100 + $successful * 10 + $failed;
    echo $result;
}

