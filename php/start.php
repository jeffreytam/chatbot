<pre>
<?php
include __DIR__ . '/config.php';

$conn = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function getClientStatus() {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'http://jtan.us:2000/openwx/check_client');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $resp = curl_exec($ch);

    if(!$resp) {
      die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    } else {
      // echo "Response HTTP Status Code : " . curl_getinfo($ch, CURLINFO_HTTP_CODE);
      // echo "\nResponse HTTP Body : " . $resp;
    }

    curl_close($ch);

    return $resp;
}

$resp = getClientStatus();
$json = json_decode($resp, true);

if ($json['code'] == 0) {
    $clients = $json['client'];
    $accountset = array_column($clients, 'account');
} else {
    $accountset = array();
}

$pool = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10'];

$client = False;

foreach ($pool as $key => $value) {
    if (!in_array($value, $accountset)) {
        echo "Client [" . $value . "] is available to start." . PHP_EOL;
        $client = $value;
        break;
    }
}

if ($client) {
    echo "\nStarting client [".$client."]..." . PHP_EOL;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://localhost:2000/openwx/start_client?client=' . $client);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $resp = curl_exec($ch);

    if(!$resp) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    } else {
        // echo "Response HTTP Status Code : " . curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // echo "Response: " . $resp . PHP_EOL;
    }
    curl_close($ch);

    echo "\nClient [" . $client . "] started and you can go back." . PHP_EOL;

} else {
    echo "\nNo available slot for new clients" . PHP_EOL;
}
?>
</pre>