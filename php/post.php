<?php
include __DIR__ . '/config.php';
include __DIR__ . '/functions.php';

$conn = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($json = file_get_contents('php://input')) {
    $data = json_decode($json, true);
    $json = $conn->real_escape_string($json);

    $client = $_GET['client'];

    $sql = "INSERT INTO `debug` (`id`, `client`, `timestamp`, `json`) VALUES (NULL, '{$client}', CURRENT_TIMESTAMP, '{$json}');";
    // $conn->query($sql);

    $post_type = $data['post_type'];

    switch ($post_type) {
        case 'event':
            $event = $data['event'];
            $params = $conn->real_escape_string(json_encode($data['params'], JSON_UNESCAPED_UNICODE));
            var_dump($params);
            $sql = "INSERT INTO `event` (`sid`, `client`, `time`, `event`, `params`) 
                VALUES (NULL, '{$client}', CURRENT_TIMESTAMP, '{$event}', '{$params}')";
            $conn->query($sql);
            break;

        case 'receive_message':
            $id = $data['id'];
            $time = $data['time'];
            $type = $data['type'];
            $class = $data['class'];
            $format = $data['format'];
            $sql = "INSERT INTO `receive_message` (`sid`, `client`, `time`, `id`, `type`, `class`, `format`, `raw`) 
                VALUES (NULL, '{$client}', FROM_UNIXTIME({$time}), '{$id}', '{$type}', '{$class}', '{$format}', '{$json}')";
            $conn->query($sql);
            break;

        case 'send_message':
            $id = $data['id'];
            $time = $data['time'];
            $type = $data['type'];
            $class = $data['class'];
            $format = $data['format'];
            $sql = "INSERT INTO `send_message` (`sid`, `client`, `time`, `id`, `type`, `class`, `format`, `raw`) 
                VALUES (NULL, '{$client}', FROM_UNIXTIME({$time}), '{$id}', '{$type}', '{$class}', '{$format}', '{$json}')";
            $conn->query($sql);
            break;

        default:
            // echo $post_type;
            break;
    }

}