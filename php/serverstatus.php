<!DOCTYPE html>
<html>
<head>
<style>
table {
    border-collapse: collapse;
    width: 50%;
}
th {
    padding: 4px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
td {
    padding: 4px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}
tr:hover {background-color: #f5f5f5}
</style>
</head>
<body>
<div style="overflow-x:auto;">
<table>
<?php
function getClientStatus() {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'http://jtan.us:2000/openwx/check_client');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $resp = curl_exec($ch);

    if(!$resp) {
      die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    } else {
      // echo "Response HTTP Status Code : " . curl_getinfo($ch, CURLINFO_HTTP_CODE);
      // echo "\nResponse HTTP Body : " . $resp;
    }

    curl_close($ch);

    return $resp;
}

$resp = getClientStatus();

$json = json_decode($resp, true);

if ($json['code'] == 0) {
    $clients = $json['client'];
}

echo "<tr>
        <th>Client</th>
        <th>Port</th>
        <th>State</th>
        <th>Start Time</th>
        <th>Operation</th>
      </tr>";

foreach ($clients as $key => $value) {
    switch ($value['state']) {
        case 'running':
            $operation = "<a href=\"http://jtan.us:2000/openwx/stop_client?client={$value['account']}\">Stop</a>";
            break;
        
        case 'scaning':
            $operation = "<a href=\"http://jtan.us:2000/openwx/get_qrcode?client={$value['account']}\">QR Code</a>";
            break;

        case 'loading':
            $operation = "Generating QR Code";
            break;

        case 'updating':
            $operation = "Loading contacts";
            break;

        case 'confirming':
            $operation = "Confirm on your phone";
            break;

        default:
            # code...
            break;
    }
    echo "<tr>";
    echo "<td>" . $value['account'] . "</td>";
    echo "<td>" . $value['port'] . "</td>";
    echo "<td>" . $value['state'] . "</td>";
    echo "<td>" . date("Y-m-d H:i:s", $value['start_time']) . "</td>";
    echo "<td>" . $operation . "</td>";
    echo "</tr>";
}


?>
</table>
</div>
<p>
<a href="http://jtan.us:8008/start.php">Start a new client</a>
</p>
</body>
</html>
