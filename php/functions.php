<?php

function getUserInfo($client) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://localhost:2000/openwx/get_user_info?client='.$client);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $resp = curl_exec($ch);

    if(!$resp) {
      die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    } else {
      $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    }
    curl_close($ch);
    if ($status_code == 200) {
        return $resp;
    } else {
        return false;
    }
    
}

function updateUserInfo($json, $conn) {
    $user = json_decode($json, true);
    foreach ($user as $key => $value) {
        $user[$key] = $conn->real_escape_string($value);
    }

    $sql = "SELECT * FROM `users` WHERE uid = {$user['uid']}";

    $result = $conn->query($sql);

    if ($result->num_rows == 0) {
        $sql = "INSERT INTO `users` (`sid`, `account`, `city`, `display`, `displayname`, `id`, `markname`, `name`, `province`, `sex`, `signature`, `uid`) 
                VALUES (NULL, '{$user['account']}', '{$user['city']}', '{$user['display']}', '{$user['displayname']}', '{$user['id']}', '{$user['markname']}', '{$user['name']}', '{$user['province']}', '{$user['sex']}', '{$user['signature']}', '{$user['uid']}')";
        echo $sql;
        $conn->query($sql);
    } else {
        $sql = "UPDATE `users` SET `account` = '{$user['account']}', `city` = '{$user['city']}', `display` = '{$user['display']}', `displayname` = '{$user['displayname']}', `id` = '{$user['id']}', `markname` = '{$user['markname']}', `name` = '{$user['name']}', `province` = '{$user['province']}', `sex` = '{$user['sex']}', `signature` = '{$user['signature']}', `uid` = '{$user['uid']}' WHERE `users`.`uid` = {$user['uid']}";
        echo $sql;
        $conn->query($sql);
    }
}